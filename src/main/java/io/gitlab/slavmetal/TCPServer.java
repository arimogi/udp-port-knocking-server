/*******************************************************************************
 * Copyright (c) 2018 SlavMetal.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the accompanying LICENSE file.
 *******************************************************************************/
package io.gitlab.slavmetal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class for simple TCP communication with the client.
 */
class TCPServer implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(TCPServer.class);
    private ServerSocket tcpServerSocket;   // TCP socket
    private InetAddress clientAddress;      // For an additional check of client's address

    /**
     * @param clientAddress         client's address.
     * @throws UnknownHostException when client's address can't be found.
     */
    TCPServer(String clientAddress) throws UnknownHostException {
        this.clientAddress = InetAddress.getByName(clientAddress);
        // Use automatically chosen port
        try {
            this.tcpServerSocket = new ServerSocket(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Just some dummy functionality to demonstrate simple communication via TCP.
     */
    @Override
    public void run() {
        boolean isAddressRight = false;
        while (!isAddressRight) {
            try (Socket tcpSocket = tcpServerSocket.accept();
                 ObjectOutputStream output = new ObjectOutputStream(tcpSocket.getOutputStream());
                 ObjectInputStream input = new ObjectInputStream(tcpSocket.getInputStream())) {
                logger.info("TCP server got connection on port " + tcpSocket.getLocalPort());

                // Check if client's address is the same as address we were waiting for
                if (this.clientAddress.equals(((InetSocketAddress) tcpSocket.getRemoteSocketAddress()).getAddress())) {
                    isAddressRight = true;
                } else {
                    logger.error("Client which should connect to this port and real client's addresses are different, skipping");
                    continue;
                }

                // Get message from client and send reply back
                String inputMessage = (String) input.readObject();
                logger.info("Received string from client: " + inputMessage);
                output.writeObject(MD5(inputMessage));
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Represents String as an MD5 hash.
     * @param str   input string.
     * @return      MD5 string of the input.
     */
    private String MD5(String str){
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(StandardCharsets.UTF_8.encode(str));
            return String.format("%032x", new BigInteger(1, md5.digest()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return server's TCP port.
     */
    int getPort() {
        return this.tcpServerSocket.getLocalPort();
    }
}
