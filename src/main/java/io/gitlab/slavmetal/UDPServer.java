package io.gitlab.slavmetal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketException;
import java.util.*;

/**
 * Starts threads to listen incoming connections.
 */
class UDPServer {
    private final Logger logger = LoggerFactory.getLogger(UDPServer.class);
    // Sequence, after which TCP port will be open
    private final List<String> portSequence;
    // Unique UDP ports to open
    private final Set<String> ports;
    // Store threads with sockets
    private List<Thread> serverThreads = new ArrayList<>();
    // Store current knocks of clients with Key as ip:port and Value as ArrayList of knocked ports
    private Map<String, ArrayList<Integer>> usersKnocksMap = new HashMap<>();

    /**
     * Default constructor.
     * @param portSequence sequence of ports after which TCP port will be open for the user.
     */
    UDPServer(List<String> portSequence) {
        this.portSequence = portSequence;
        logger.info("Received UDP ports sequence: " + this.portSequence);
        this.ports = new HashSet<>(portSequence);
        logger.info("Following UDP ports will be open: " + this.ports);
    }

    /**
     * Starts and controls threads with sockets.
     */
    void startServer(){
        for (String port : ports){
            try {
                Thread tmpThread = new Thread(new SocketThread(Integer.parseInt(port), portSequence, usersKnocksMap));
                serverThreads.add(tmpThread);
                tmpThread.start();
            } catch (SocketException e) {
                e.printStackTrace();
                logger.warn("Interrupting all threads");
                for (Thread thread : serverThreads)
                    thread.interrupt();
                System.exit(0);
            }
        }
    }
}
